<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 5/26/17
 * Time: 11:02 AM
 */

if (!defined('AUTHORIZED')) die();

if (!defined('DS')) define('DS', DIRECTORY_SEPARATOR);
if (!defined('NO_UI')) {
    if (!session_id()) session_start();
    include_once 'exceptions.php';
}

include_once dirname(__DIR__) .'/vendor/autoload.php';
include_once 'auto_loader_functions.php';

// include directories
add_subdirectory_to_path('');
add_subdirectory_to_path('controllers');
add_subdirectory_to_path('helpers');
add_subdirectory_to_path('models');
add_directory_to_path(dirname(__DIR__) . '/site_template/lib');

// load libraries used by most pages
include_once 'globals.php';
include_once 'include_twig.php';