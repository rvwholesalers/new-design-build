<?php

namespace Helpers;

use Aptoma\Twig\Extension\MarkdownEngineInterface;
use ParsedownExtra;

/**
 * ParsedownExtraEngine.php
 *
 * Maps erusev/parsedown-extra to Aptoma\Twig Markdown Extension
 */
class ParsedownExtraEngine implements MarkdownEngineInterface
{
    /**
     * @var ParsedownExtra
     */
    protected $engine;

    /**
     * @param string|null $instanceName
     */
    public function __construct($instanceName = null)
    {
        $this->engine = ParsedownExtra::instance($instanceName);
    }

    /**
     * {@inheritdoc}
     */
    public function transform($content)
    {
        return $this->engine->parse($content);
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'erusev/parsedown-extra';
    }
}
