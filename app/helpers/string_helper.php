<?php

namespace Helpers;

if (!defined('AUTHORIZED')) die();

/**
 *
 * Name: string_helper.php
 * Description: String functions
 *
 * Author: Phil Hopper
 * Created: May 7, 2013
 *
 */
class StringHelper
{

    /**
     * Determine if it is possible for the haystack to contain the needle
     * @param string $haystack
     * @param string $needle
     * @return boolean
     */
    private static function may_contain($haystack, $needle)
    {
        $strlen = strlen($haystack);
        $test_len = strlen($needle);
        if ($strlen < 1) {
            return false;
        }
        if ($test_len < 1) {
            return false;
        }
        if ($test_len > $strlen) {
            return false;
        }

        return true;
    }

    public static function BeginsWith($haystack, $needle)
    {
        if (!self::may_contain($haystack, $needle)) {
            return false;
        }
        if ($haystack == $needle) {
            return true;
        }
        return (strpos($haystack, $needle) === 0);
    }

    /**
     * @param string $haystack
     * @param string[] $needles
     * @return bool
     */
    public static function BeginsWithAny($haystack, $needles)
    {
        foreach($needles as $needle) {
            if (self::BeginsWith($haystack, $needle)) {
                return true;
            }
        }

        return false;
    }

    public static function EndsWith($haystack, $needle)
    {
        if (!self::may_contain($haystack, $needle)) {
            return false;
        }
        if ($haystack == $needle) {
            return true;
        }
        return substr_compare($haystack, $needle, -(strlen($needle))) === 0;
    }

    public static function Contains($haystack, $needle)
    {
        if (!self::may_contain($haystack, $needle)) {
            return false;
        }
        if ($haystack == $needle) {
            return true;
        }
        return (strpos($haystack, $needle) !== false);
    }

    /**
     * Combines two or more strings using DIRECTORY_SEPARATOR.
     *
     * @return string The path properly combined
     */
    public static function PathCombine()
    {
        $sep = DIRECTORY_SEPARATOR;
        $not_sep = ($sep == '/') ? '\\' : '/';

        $segments = func_get_args();
        $seg_cnt = count($segments);

        $return_val = '';

        // check for windows vs unix
        for ($i = 0; $i < $seg_cnt; $i++) {
            $segments[$i] = str_replace($not_sep, $sep, $segments[$i]);

            // remove trailing DIRECTORY_SEPARATOR from all segments
            while (self::EndsWith($segments[$i], $sep)) {
                $segments[$i] = substr($segments[$i], 0, strlen($segments[$i]) - 1);
            }

            // remove leading DIRECTORY_SEPARATOR from all segments except the first
            if ($i != 0) {
                while (self::BeginsWith($segments[$i], $sep)) {
                    $segments[$i] = substr($segments[$i], 1);
                }
            }

            // combine the segments
            if (!empty($segments[$i])) {

                if (empty($return_val)) {
                    $return_val = $segments[$i];
                } else {
                    $return_val .= $sep . $segments[$i];
                }
            }
        }

        return $return_val;
    }

    /**
     * Combines two or more strings using DIRECTORY_SEPARATOR and converts to the absolute path
     *
     * @return string The canonical path, or false if path not found.
     */
    public static function RealPathCombine()
    {
        $args = func_get_args();
        $path = call_user_func_array('self::PathCombine', $args);
        return realpath($path);
    }

    public static function GUID()
    {
        if (function_exists('com_create_guid') === true) {
            /** @noinspection PhpUndefinedFunctionInspection */
            return trim(com_create_guid(), '{}');
        }

        return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535),
            mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
    }

    public static function FormatPhone($value)
    {
        $regex = '/([^0-9])/';
        $stripped = preg_replace($regex, '', $value);

        switch (strlen($stripped)) {
            case 11:
            case 10:
                return '(' . substr($stripped, 0, 3) . ') ' . substr($stripped, 3, 3) . '-' . substr($stripped, 6);

            case 7:
                return substr($stripped, 0, 3) . '-' . substr($stripped, 3);
        }

        // if not able to format, return the original value
        return $value;
    }
}
