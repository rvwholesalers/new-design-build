<?php
/**
 * Name: twig_filters.php
 * Description:
 *
 * Created by PhpStorm.
 *
 * Author: Phil Hopper
 * Date:   8/21/15
 * Time:   3:22 PM
 */

namespace Helpers;

class TwigFilters
{

    public static function decode_json($jsonString)
    {
        return json_decode($jsonString, true);
    }

    public static function ceil($number)
    {
        return ceil($number);
    }

    public static function currency_format($value, $decimal_places, $hide_zero = false, $hide_symbol = true)
    {
        if (empty($value) && $hide_zero) {
            return '';
        }
        return NumberHelper::format_currency($value, $decimal_places, !$hide_symbol);
    }

    public static function phone_format($value, $sep = '.')
    {
        // remove non-numeric characters
        $regex = '/([^0-9])/';
        $value = preg_replace($regex, '', $value);


        if (strlen($value) > 10) {
            $value = substr($value, strlen($value) - 10);
        }

        if (strlen($value) == 7) {
            return substr($value, 0, 3) . $sep . substr($value, 3);
        }

        return substr($value, 0, 3) . $sep . substr($value, 3, 3) . $sep . substr($value, 6);
    }

    public static function url_category($value, $category)
    {
        if (strpos($value, '?') === false) {
            return $value . '/new-rvs/index.php?category=' . $category;
        }
        else {
            return $value . '&category=' . $category;
        }
    }
}
