<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 11/21/16
 * Time: 7:51 AM
 */

namespace Helpers;

use Database;

if (!defined('AUTHORIZED')) die();

class SqlHelper
{
    /**
     * @param string $file_name
     * @param Database $db
     * @param array|null $params
     * @param bool $do_not_escape
     * @return string
     */
    public static function LoadSQL($file_name, $db, $params=null, $do_not_escape=false)
    {
        if (is_file($file_name)) {
            $sql = file_get_contents($file_name);
        } else {
            $sql = file_get_contents(StringHelper::PathCombine(SQL_DIR, $file_name));
        }

        if (!empty($params)) {
            foreach($params as $key => $value) {
                if ($do_not_escape) {
                    $sql = str_replace($key, $value, $sql);
                }
                else {
                    $sql = str_replace($key, $db->cn->real_escape_string($value), $sql);
                }
            }
        }

        return $sql;
    }
}