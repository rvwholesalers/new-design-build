<?php
/**
 *
 * Name: request.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: Oct 19, 2013
 *
 */

namespace Helpers;

if (!defined('AUTHORIZED')) die();

class Request
{
    /** @var  string[] */
    private static $url_parts;

    /**
     * Returns the $_SERVER[$variable_name] value as a string
     * @param string $variable_name
     * @return string
     */
    public static function IsServerSet($variable_name)
    {
        return filter_input(INPUT_SERVER, $variable_name, FILTER_UNSAFE_RAW) !== null;
    }

    /**
     * Returns the $_SERVER[$variable_name] value as a string
     * @param string $variable_name
     * @return string
     */
    public static function ServerStr($variable_name)
    {
        return (string)filter_input(INPUT_SERVER, $variable_name, FILTER_UNSAFE_RAW);
    }

    /**
     * Returns true if the $_POST[$variable_name] is set
     * @param string $variable_name
     * @return bool
     */
    public static function IsPostSet($variable_name)
    {
        return filter_input(INPUT_POST, $variable_name, FILTER_UNSAFE_RAW) !== null;
    }

    /**
     * Returns the $_POST[$variable_name] value as a string
     * @param string $variable_name
     * @return string
     */
    public static function PostStr($variable_name)
    {
        return (string)filter_input(INPUT_POST, $variable_name, FILTER_UNSAFE_RAW);
    }

    /**
     * Returns the $_POST[$variable_name] value as an int
     * @param string $variable_name
     * @return int
     */
    public static function PostInt($variable_name)
    {
        return (int)filter_input(INPUT_POST, $variable_name, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     *
     * @param string $variable_name
     * @param float $default
     * @param int $decimal_places
     * @return float
     */
    public static function PostFloat($variable_name, $default = 0.0, $decimal_places = null)
    {
        $val = filter_input(INPUT_POST, $variable_name, FILTER_SANITIZE_NUMBER_FLOAT, FILTER_FLAG_ALLOW_FRACTION);

        if (empty($val)) $val = $default;

        if ($decimal_places != null) $val = number_format($val, $decimal_places);

        return (float)$val;
    }

    /**
     *
     * @param string $variable_name
     * @return int a timestamp on success, <b>null</b> otherwise.
     */
    public static function PostDate($variable_name)
    {
        $val = self::PostStr($variable_name);

        if (empty($val)) return null;
        return strtotime($val);
    }

    /**
     * Returns true if the $_GET[$variable_name] is set
     * @param string $variable_name
     * @return bool
     */
    public static function IsGetSet($variable_name)
    {
        return filter_input(INPUT_GET, $variable_name, FILTER_UNSAFE_RAW) !== null;
    }

    /**
     * Returns the $_GET[$variable_name] value as a string
     * @param string $variable_name
     * @return string
     */
    public static function GetStr($variable_name)
    {
        return (string)filter_input(INPUT_GET, $variable_name, FILTER_UNSAFE_RAW);
    }

    /**
     * Returns the $_GET[$variable_name] value as an int
     * @param string $variable_name
     * @return int
     */
    public static function GetInt($variable_name)
    {
        return (int)filter_input(INPUT_GET, $variable_name, FILTER_SANITIZE_NUMBER_INT);
    }

    /**
     * Returns the $_GET[$variable_name] value as a float
     * @param string $variable_name
     * @param float $default
     * @param int $decimal_places
     * @return float
     */
    public static function GetFloat($variable_name, $default = 0.0, $decimal_places = null)
    {
        $val = filter_input(INPUT_GET, $variable_name, FILTER_SANITIZE_NUMBER_FLOAT);

        if (empty($val)) $val = $default;

        if ($decimal_places != null) $val = number_format($val, $decimal_places);

        return (float)$val;
    }

    /**
     * Returns the URL as an array of segments split on the slashes
     * @return array|\string[]
     */
    public static function UrlParts()
    {
        if (empty(self::$url_parts)) {

            $uri = self::ServerStr('REQUEST_URI');

            // remove the query string
            $pos = strpos($uri, '?');
            if ($pos !== false) {
                $uri = substr($uri, 0, $pos);
            }

            self::$url_parts = preg_split('@/@', $uri, NULL, PREG_SPLIT_NO_EMPTY);
        }

        return self::$url_parts;
    }
}
