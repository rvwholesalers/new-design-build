<?php
/**
 * Name: cache.php
 * Description:
 *
 * Created by PhpStorm.
 *
 * Author: Phil Hopper
 * Date:   11/1/14
 * Time:   2:55 PM
 */

namespace Controllers;

use Helpers\MiscFunctions;

if (!defined('AUTHORIZED')) die();

class Cache
{
    private static $cacheDuration = 7200; // one day = 86400 seconds, one hour = 3600 seconds

    private $cacheFolder;
    private $cacheName;

    function __construct($cacheName)
    {
        $this->cacheName = $cacheName;
        $this->cacheFolder = dirname(dirname(__DIR__)) . DS . 'cache' . DS . $cacheName;

        // make sure the cache directory exists
        if (!file_exists($this->cacheFolder))
            mkdir($this->cacheFolder, 0775, true);
    }

    function clear_cache()
    {
        MiscFunctions::clear_directory($this->cacheFolder, false);
    }

    /**
     * @param string $fileName
     * @return null|string Returns null if not found or expired
     */
    function get_cached_file($fileName)
    {
        $fileName = $this->cacheFolder . DS . $fileName;

        // if the file does not exist, return null
        if (!is_file($fileName)) return null;

        // check if expired
        $fileCreated = filemtime($fileName);
        if (($fileCreated + self::$cacheDuration) < time()) {
            unlink($fileName);
            return null;
        }

        return file_get_contents($fileName);
    }

    /**
     * @param string $fileName
     * @param string $contents
     */
    function put_cache_file($fileName, $contents)
    {
        $fileName = $this->cacheFolder . DS . $fileName;
        $cache_dir = dirname($fileName);
        if (!is_dir($cache_dir)) {
            mkdir($cache_dir, 0775, true);
        }
        file_put_contents($fileName, $contents);
    }
}
