<?php
/**
 * This class renders a page using templates
 * Name: page.php
 * Description:
 *
 * Author: Phil Hopper
 * Created: Mar 31, 2014
 *
 */

namespace Controllers;

use Aptoma\Twig\Extension\MarkdownExtension;
use Aptoma\Twig\TokenParser\MarkdownTokenParser;
use Exception;
use Helpers\MiscFunctions;
use Helpers\ParsedownExtraEngine;
use Helpers\StringHelper;
//use Pages\Specials;
use Twig_Environment;
use Twig_Loader_Filesystem;
use Twig_SimpleFilter;
use Twig_SimpleFunction;

class Page
{
    // Hold an instance of the class
    private static $instance;
    private static $absolute_instance;
    private $loader;
    private $twig;

    private function __construct($allow_absolute_path)
    {
        // initialize twig
        $paths = array(
            TEMPLATE_MODULES,
            TEMPLATE_PAGES,
            TEMPLATE_CONTROLS,
            TEMPLATE_ROOT
        );
        if ($allow_absolute_path) {
            $paths[] = '/';
        }
        $this->loader = new Twig_Loader_Filesystem($paths);
        $this->twig = new Twig_Environment($this->loader);

        // markdown support
        $engine = new ParsedownExtraEngine();
        $this->twig->addExtension(new MarkdownExtension($engine));
        $this->twig->addTokenParser(new MarkdownTokenParser($engine));

        // set up twig cache
        if (!defined('DEBUG')) {
            $cache_path = StringHelper::RealPathCombine(CACHE_DIR, 'twig');
            $this->twig->setCache($cache_path);
            $this->twig->disableAutoReload();
        }

        // add our twig functions
        foreach (get_class_methods('\Helpers\TwigFunctions') as $method) {
            $this->twig->addFunction(new Twig_SimpleFunction($method, '\Helpers\TwigFunctions::' . $method));
        }

        // add our twig filters
        foreach (get_class_methods('\Helpers\TwigFilters') as $filter) {
            $this->twig->addFilter(new Twig_SimpleFilter($filter, '\Helpers\TwigFilters::' . $filter));
        }

        // add twig globals
        $this->twig->addGlobal('WEB_ROOT', WEB_ROOT);
        $this->twig->addGlobal('PHONE_NUMBER', PHONE_NUMBER);
        $this->twig->addGlobal('TOLL_FREE', TOLL_FREE);
        $this->twig->addGlobal('STREET_ADDRESS', STREET_ADDRESS);
        $this->twig->addGlobal('CITY_STATE_ZIP', CITY_STATE_ZIP);
//        $this->twig->addGlobal('SITE_WIDE_BANNER', (empty(Specials::$SiteWideBanner) ? '' : Specials::$SiteWideBanner));
    }

    /**
     *
     * @param bool $allow_absolute_path
     * @return Page
     */
    public static function Get_Page($allow_absolute_path=false)
    {
        if ($allow_absolute_path) {

            if (!isset(self::$absolute_instance)) {
                $c = __CLASS__;
                self::$absolute_instance = new $c(true);
            }

            return self::$absolute_instance;
        } else {

            if (!isset(self::$instance)) {
                $c = __CLASS__;
                self::$instance = new $c(false);
            }

            return self::$instance;
        }
    }

    public function prependPath($path)
    {
        $this->loader->prependPath($path);
    }

    /**
     * Returns the html after putting the page together
     * @param string $template_file The file to use as the starting point
     * @param array $vars The key is the name of the variable, the value is the value of the variable.
     * @return string
     */
    public function Construct_Page($template_file, $vars = array())
    {
        $tpl = $this->twig->loadTemplate($template_file);

        $html = null;

        try {
            $html = $tpl->render($vars);

            // set web root
            $html = str_replace("~/", WEB_ROOT . "/", $html);

        } catch (Exception $e) {
            trigger_error($e->getMessage(), E_USER_ERROR);
        }

        return $html;
    }

    /**
     * Output page to the browser
     * @param string $template_file The file to use as the starting point
     * @param array $vars The key is the name of the variable, the value is the value of the variable.
     */
    public function Render_Page($template_file, $vars = array())
    {
        // clean up any stuff not part of the page
        if (ob_get_contents()) {
            ob_clean();
        }

        $html = $this->Construct_Page($template_file, $vars);

        // clean up any messes
        if (ob_get_contents()) {
            ob_clean();
        }

        echo $html;
    }

    /**
     * We needed to implement this ourselves since `clearCacheFiles()` was deprecated
     */
    public function Clear_Cache()
    {
        $cache_dir = $this->twig->getCache();
        if (is_dir($cache_dir)) {
            MiscFunctions::clear_directory($cache_dir, false);
        }
    }
}
