<?php

/* set this to prevent the time zone warning */
date_default_timezone_set('America/New_York');

function debug_error_handler(/** @noinspection PhpUnusedParameterInspection */
    $err_level, $err_message, $err_file = '', $err_line = -1, $err_context = null) {

    // display the error - for debugging
    echo "\n<br />Error: [$err_level] $err_message\n<br />\n<br />File: $err_file, $err_line\n<br />\n<br />";
    debug_print_backtrace();

    // stop
    die();
}

function default_error_handler(/** @noinspection PhpUnusedParameterInspection */
    $err_level, $err_message, $err_file = '', $err_line = -1, $err_context = null) {

    // display generic message - for production
    $msg = "<br />Something unexpected happened and we were not able to complete your request.<br />";
    $msg = '<div style="text-align:center;width:100%;"><div style="text-align:left;display: inline-block;">' . $msg . '</div></div>';
    echo $msg;

    // stop
    die();
}

/**
 *
 * @param Exception $exception
 */
function debug_exception_handler($exception) {

    echo "\n<br />Error: [{$exception->getCode()}] {$exception->getMessage()}\n<br />\n<br />File: {$exception->getFile()}, {$exception->getLine()}\n<br />\n<br />";

    echo $exception->getTraceAsString();
}

/**
 *
 * @param Exception $exception
 */
function default_exception_handler(/** @noinspection PhpUnusedParameterInspection */ $exception) {

    // display generic message - for production
    $msg = "<br />Something unexpected happened and we were not able to complete your request.<br />";
    $msg = '<div style="text-align:center;width:100%;"><div style="text-align:left;display: inline-block;">' . $msg . '</div></div>';

    echo $msg;
}

function enable_error_handling() {

    // debug or production
    $host = filter_input(INPUT_SERVER, 'HTTP_HOST', FILTER_UNSAFE_RAW);
    if (empty($host))
        $debug = true; // probably running from console
    elseif (strpos($host, 'localhost') !== false)
        $debug = true;
    elseif (strpos($host, 'test.') === 0)
        $debug = true;
    else
        $debug = false;
    
    if ($debug) {

        /* For debugging only */
        //set_error_handler("debug_error_handler", E_ALL);
        set_error_handler("debug_error_handler", (E_ALL | E_STRICT));
        set_exception_handler('debug_exception_handler');
    }
    else {

        /* Normal running */
        set_error_handler("default_error_handler", E_ALL & ~ (E_NOTICE | E_USER_NOTICE | E_WARNING | E_USER_WARNING));
        set_exception_handler('default_exception_handler');
    }
}

function disable_error_handling() {

    restore_error_handler();
    restore_exception_handler();
}

enable_error_handling();
