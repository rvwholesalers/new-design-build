<?php
/**
 *
 * Loads Twig template engine
 *
 * Created Oct 18, 2013 by Phil Hopper
 *
 */

use Helpers\StringHelper;

if (!defined('AUTHORIZED')) die();

$dir = StringHelper::RealPathCombine(PROJECT_DIR, 'templates');

// TEMPLATE_MODULES is where shared headers, footers, etc are located
if (!defined('TEMPLATE_MODULES')) {
    $path = $dir . DS . 'modules';
    define('TEMPLATE_MODULES', realpath($path));
}

// TEMPLATE_CONTROLS is where web controls are located
if (!defined('TEMPLATE_CONTROLS')) {
    $path = $dir . DS . 'controls';
    define('TEMPLATE_CONTROLS', realpath($path));
}

// TEMPLATE_PAGES is where page layout files are located
if (!defined('TEMPLATE_PAGES')) {
    $path = $dir . DS . 'pages';
    define('TEMPLATE_PAGES', realpath($path));
}

// TEMPLATE_ROOT is where the shared layout files can be found
if (!defined('TEMPLATE_ROOT')) {
    $path = RVWTemplate::twig_file_directory();
    if (is_dir($path)) {
        define('TEMPLATE_ROOT', realpath($path));
    }
}
