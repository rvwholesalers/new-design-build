<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 10/7/16
 * Time: 3:14 PM
 */

use Controllers\Cache;
use Helpers\StringHelper;

define('WEB_ROOT', '');
define('PROJECT_DIR', dirname(__DIR__));
define('APP_DIR', StringHelper::RealPathCombine(PROJECT_DIR, 'app'));
define('CONFIG_DIR', StringHelper::RealPathCombine(PROJECT_DIR, 'config'));
define('CACHE_DIR', StringHelper::RealPathCombine(PROJECT_DIR, 'cache'));
define('PUBLIC_DIR', StringHelper::RealPathCombine(PROJECT_DIR, 'public'));
define('SQL_DIR', StringHelper::RealPathCombine(APP_DIR, 'sql'));

define('MAIN_MENU', StringHelper::RealPathCombine(APP_DIR, 'menus', 'main_menu.yaml'));

define('WATERMARK', 'RV Wholesalers');
define('SITE_NAME', 'RV Wholesalers');
define('PHONE_NUMBER', '19378439000');
define('TOLL_FREE', '18778774494');
define('STREET_ADDRESS', '530 North Main Street');
define('CITY_STATE_ZIP', 'Lakeview, Ohio 43331');

/** @var \Controllers\Cache $dataCache */
$dataCache = new Cache('data');

// for old site support
define('MODULES_DIR', StringHelper::RealPathCombine(PROJECT_DIR, 'modules'));
