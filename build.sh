#!/usr/bin/env bash

npm update

mkdir -p "public/lib/jquery"
cp -u "node_modules/jquery/dist/jquery.js" "public/lib/jquery/jquery.js"
cp -u "node_modules/jquery/dist/jquery.min.js" "public/lib/jquery/jquery.min.js"
cp -u "node_modules/jquery/dist/jquery.min.map" "public/lib/jquery/jquery.min.map"

./less.sh
./tsc.sh
