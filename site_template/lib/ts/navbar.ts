/**
 *
 * @param menu
 */
function showThisMenu(menu: HTMLLIElement) {

    // hide other open menu drop downs
    $('nav ul.menu > li').not(menu).find('ul.dropdown-menu:visible').fadeOut('fast');

    $(menu).find('ul').fadeToggle('fast');
}

$().ready(function () {

    // stick in the fixed 100% height behind the navbar but don't wrap it
    $('#slide-nav.navbar-inverse').after($('<div class="inverse" id="navbar-height-col"></div>'));

    $('#slide-nav.navbar-default').after($('<div id="navbar-height-col"></div>'));

    // Enter your ids or classes
    const toggler = '.navbar-toggle';
    const page_wrapper = '#page-content';
    const navigation_wrapper = '.navbar-header';

    const slide_width = '80%';
    const menu_neg = '-100%';
    const slide_neg = '-80%';

    $("#slide-nav").on("click", toggler, function () {

        let selected = $(this).hasClass('slide-active');
        const $slidemenu = $('#slidemenu');
        $slidemenu.stop().animate({
            left: selected ? menu_neg : '0px'
        });

        $('#navbar-height-col').stop().animate({
            left: selected ? slide_neg : '0px'
        });

        $(page_wrapper).stop().animate({
            left: selected ? '0px' : slide_width
        });

        $(navigation_wrapper).stop().animate({
            left: selected ? '0px' : slide_width
        });

        $(this).toggleClass('slide-active', !selected);
        $slidemenu.toggleClass('slide-active');

        $('#page-content, .navbar, body, .navbar-header').toggleClass('slide-active');
    });

    const selected = '#slidemenu, #page-content, body, .navbar, .navbar-header';

    $(window).on("resize", function () {

        if ($(window).width() > 767 && $('.navbar-toggle').is(':hidden')) {
            $(selected).removeClass('slide-active');
        }
    });

    // Show or hide sub-menu on click
    const $nav_ul_menu_li = $('nav ul.menu > li');
    $nav_ul_menu_li.click(function(e) {
        showThisMenu(this);
        e.stopPropagation();
    });
    $(document).click(function() {
        $('nav ul.dropdown-menu:visible').fadeOut('fast');
    });

    // // Show sub-menu on hover
    // $nav_ul_menu_li.mouseenter(function(e) {
    //     showThisMenu(this);
    //     e.stopPropagation();
    // });
    //
    // $(document).mouseenter(function() {
    //     $('nav ul.dropdown-menu:visible').fadeOut('fast');
    // });
});