function searchForThis() {

    let val = (<HTMLInputElement>document.getElementById('simple-search')).value.trim();
    if (!val) return;

    window.location.href = '/new-rvs/index.php?searchFor=' + encodeURIComponent(val);
}

$().ready(function () {

    $('#simple-search').on('keypress', function (e) {

        if (!e['keyCode']) return;

        if ((e['keyCode'] == 13) || (e['keyCode'] == 3))
            searchForThis();
    });

    $('#simple-search-button').on('click', function () { searchForThis(); });
});
