
declare var $JssorSlider$;
declare var $JssorSlideshowFormations$;
declare var $JssorSlideshowRunner$;
declare var $JssorEasing$;
declare var $JssorArrowNavigator$;
declare var $JssorThumbnailNavigator$;

interface $JssorEasing$ {
	$EaseLinear(t);
	$EaseGoBack(t);
	$EaseSwing(t);
	$EaseInQuad(t);
	$EaseOutQuad(t);
	$EaseInOutQuad(t);
	$EaseInCubic(t);
	$EaseOutCubic(t);
	$EaseInOutCubic(t);
	$EaseInQuart(t);
	$EaseOutQuart(t);
	$EaseInOutQuart(t);
	$EaseInQuint(t);
	$EaseOutQuint(t);
	$EaseInOutQuint(t);
	$EaseInSine(t);
	$EaseOutSine(t);
	$EaseInOutSine(t);
	$EaseInExpo(t);
	$EaseOutExpo(t);
	$EaseInOutExpo(t);
	$EaseInCirc(t);
	$EaseOutCirc(t);
	$EaseInOutCirc(t);
	$EaseInElastic(t);
	$EaseOutElastic(t);
	$EaseInOutElastic(t);
	$EaseInBack(t);
	$EaseOutBack(t);
	$EaseInOutBack(t);
	$EaseInBounce(t);
	$EaseOutBounce(t);
	$EaseInOutBounce(t);
	$EaseInWave(t);
	$EaseOutWave(t);
	$EaseOutJump(t);
	$EaseInJump(t);
}

interface $JssorArrowNavigator$ {

	new(arrowLeft, arrowRight, options, containerWidth, containerHeight);

	_Length;
	_CurrentIndex;
	_Options;
	_Steps;
	_ArrowWidth;
	_ArrowHeight;
	_Located;
	_Initialized;

	OnNavigationRequest(steps);
	$GetCurrentIndex();
	$SetCurrentIndex(index, virtualIndex, temp);
	$Show(hide);
	$Relocate(force);
	$Reset(length);
}

interface $JssorThumbnailNavigator$ {
	_Self;
	_Length;
	_Count;
	_CurrentIndex;
	_Options;
	_NavigationItems;
	_Width;
	_Height;
	_Lanes;
	_SpacingX;
	_SpacingY;
	_PrototypeWidth;
	_PrototypeHeight;
	_DisplayPieces;
	_Slider;
	_CurrentMouseOverIndex;
	_SlidesContainer;
	_ThumbnailPrototype;
	_Initialized;

	new(elmt, options);

	NavigationItem(item, index);

	$GetCurrentIndex();
	$SetCurrentIndex(index, virtualIndex, temp);
	$Show(hide);
	$Relocate();
	$Reset(length, items, loadingContainer);
}
