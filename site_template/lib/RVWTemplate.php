<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 7/1/16
 * Time: 4:31 PM
 */

class RVWTemplate {
    
    private static $twig_directory = null;

    public static function twig_file_directory() {

        if (empty(self::$twig_directory)) {
            $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'twig';
            self::$twig_directory = realpath($dir);
        }

        return $dir = dirname(__FILE__) . DIRECTORY_SEPARATOR . 'twig';
    }
}
