<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 9/30/16
 * Time: 4:32 PM
 *
 * Borrowed from: vendor/deployer/deployer/recipe/symfony.php
 */

require_once '../vendor/autoload.php';
require_once 'recipe/common.php';

set('default_stage', 'test');
set('repository', 'git@bitbucket.org:rvwholesalers/new-design-build.git');
set('shared_dirs', ['logs', 'cache', 'config']);
set('writable_dirs', ['cache', 'logs']);

serverList('servers.yml');

task('deploy:cache:clear', function () {

    // Clear all files from cache directory if it exists
    env('cache_dir', '{{release_path}}/cache');
    run('if [ -d "{{cache_dir}}" ]; then rm -rf {{cache_dir}}/*; fi');

})->desc('Clear the cache directory.');

//task('database:migrate', function () {
//
//    // if the data settings config file exists, run the migrations
//    env('config_file', '{{release_path}}/config/website_data_settings.json');
//    run('if [ -f "{{config_file}}" ]; then {{bin/php}} {{release_path}}/app/controllers/migrate.php; fi');
//
//})->desc('Run any needed database migrations.');

task('deploy:npm', function () {

    $releases = env('releases_list');

    if (isset($releases[1])) {
        if(run("if [ -d {{deploy_path}}/releases/{$releases[1]}/node_modules ]; then echo 'true'; fi")->toBool()) {
            run("cp --recursive {{deploy_path}}/releases/{$releases[1]}/node_modules {{release_path}}");
        }
    }

    run("cd {{release_path}} && npm install");

})->desc('Install the nodejs packages needed to compile less and typescript files.');

//task('deploy:static_symlink', function () {
//
//    // create link to the static resources in the template
//    run("ln -sfn {{release_path}}/vendor/rvwholesalers/website-template/lib/static/ {{release_path}}/public/static");
//
////    // create link to the images in btsync
////    run("ln -sfn {{deploy_path}}/shared/images/ {{release_path}}/public/img");
//
//})->desc('Creating symlink to static resources in the website template.');

task('deploy:compile:less', function () {
    run("rm -f {{release_path}}/.lastLessCheck");
    run("cd {{release_path}} && ./less.sh");
})->desc('Compile the less files into css.');

task('deploy:compile:typescript', function () {
    run("rm -f {{release_path}}/.lastTscCheck");
    run("cd {{release_path}} && ./tsc.sh");
})->desc('Compile the typescript files into js.');

//task('deploy:generate_static', function () {
//
//    run("{{bin/php}} {{release_path}}/deployer/generate_content.php");
//    run("chmod -R 0755 {{release_path}}/public");
//
//})->desc('Generate static pages.');

task('deploy:well_known_symlink', function () {

    // create link to the static resources in the template
    run("ln -sfn {{deploy_path}}/shared/.well-known/ {{release_path}}/public/.well-known");

})->desc('Creating symlink to the .well-known shared directory.');

/**
 * Main task
 */
task('deploy', [
    'deploy:prepare',
    'deploy:release',
    'deploy:update_code',
    'deploy:shared',
    'deploy:vendors',
    'deploy:npm',
    'deploy:writable',
//    'deploy:static_symlink',
    'deploy:symlink',
    'deploy:compile:less',
    'deploy:compile:typescript',
    'deploy:cache:clear',
//    'database:migrate',
//    'deploy:generate_static',
    'deploy:well_known_symlink',
    'cleanup',
])->desc('Deploy your project');

after('deploy', 'success');
