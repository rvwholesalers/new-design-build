# New Design Build

__ABSOLUTELY NO SECURITY CREDENTIALS ARE TO BE CHECKED INTO THIS REPOSITORY!__

### Setting up a development environment

1. Clone this repository.
2. Open a terminal and `cd` to the directory where you cloned the repository. Alternately you could create a project with IntelliJ IDEA and run the following commands in the IntelliJ project terminal.
3. Install the node libraries.
    ```bash
    npm install
    ```
4. Install the composer libraries.
    ```bash
    php composer.phar install
    ```
5. Create a symlink to the static content in the public directory.
    ```
    ln -s `pwd`/vendor/rvwholesalers/website-template/lib/static/ `pwd`/public/static
    ```
    
### Building

Building will copy the jquery libraries and transpile the LESS and TypeScript files.

```bash
./build.sh
```

### Compiling LESS files

```bash
./less.sh
```

### Compiling TypeScript files

```bash
./tsc.sh
```

### Deploying to test server

The test server is at [https://vr.rvwholesalers.com](https://vr.rvwholesalers.com).

This command will publish the `develop` branch from Bitbucket to the web. You also need to have the `vr_design_team.pem`
file in your `.ssh` directory:

```bash
make test
```
