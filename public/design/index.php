<?php
/**
 * Created by IntelliJ IDEA.
 * User: phil
 * Date: 5/26/17
 * Time: 10:43 AM
 */

use Controllers\Page;
use Helpers\Request;
use Symfony\Component\Yaml\Yaml;

define('AUTHORIZED', 'yes');

include_once dirname(dirname(__DIR__)) . '/app/app_start.php';

$parts = Request::UrlParts();

$vars = array();
$vars['menu'] = Yaml::parse(file_get_contents(MAIN_MENU));
$vars['step_text'] = array(1 => 'Trailer Type', 2 => 'Brand', 3 => 'Floorplan', 4 => 'Options', 5 => 'Quote');

if (in_array('step2', $parts)) {
    $template = 'step2.page.html.twig';
    $vars['design_step'] = 2;
}
else {
    // default is step 1
    $template = 'step1.page.html.twig';
    $vars['design_step'] = 1;
}

Page::Get_Page()->Render_Page($template, $vars);
