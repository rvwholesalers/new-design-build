#!/usr/bin/env bash

thisDir="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
ext=".css"
path="public/css"
file="${thisDir}/less/styles.less"

destination="${file/.less/$ext}"
destination="${destination/less/$path}"

printf "Compiling LESS files... "
"${thisDir}/node_modules/.bin/lessc" --include-path=${thisDir}/less/include --clean-css="--s1 -b" --no-color "$file" "$destination"

printf "finished.\n"
